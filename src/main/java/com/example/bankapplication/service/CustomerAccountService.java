package com.example.bankapplication.service;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.springframework.stereotype.Service;

import com.example.bankapplication.dto.AddAccountDTO;
import com.example.bankapplication.dto.AddaccountResponseDTO;
import com.example.bankapplication.dto.FavAccountDTO;
import com.example.bankapplication.dto.LoginDTO;
import com.example.bankapplication.dto.ResponseDTO;
import com.example.bankapplication.exception.AccountNotFoundException;
import com.example.bankapplication.exception.CustomerNotFoundException;
import com.example.bankapplication.exception.FavAccountExistsException;
import com.example.bankapplication.exception.FavAccountLimitReachedException;
import com.example.bankapplication.exception.InvalidCustomerException;
import com.example.bankapplication.exception.RestTemplateException;

@Service
public interface CustomerAccountService {
	
	public AddaccountResponseDTO addAccount(long customerId,@Valid AddAccountDTO addAccountDto) throws RestTemplateException,FavAccountLimitReachedException,FavAccountExistsException,CustomerNotFoundException;

	public List<FavAccountDTO> fetchCustomerFavAccounts(long customerId) throws AccountNotFoundException,CustomerNotFoundException;

	public List<FavAccountDTO> fetchCustomerFavAccountsPaginated(long customerId,int page)throws AccountNotFoundException,CustomerNotFoundException;

	public ResponseDTO customerLogin(@Valid LoginDTO loginDto, HttpServletRequest request) throws InvalidCustomerException ;
	
	public ResponseDTO deleteFavAccount(long customerId, long accountId) throws InvalidCustomerException,CustomerNotFoundException,AccountNotFoundException;
}
