package com.example.bankapplication.service;

import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.LoggerFactory;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.web.client.HttpClientErrorException;
import org.springframework.web.client.HttpServerErrorException;
import org.springframework.web.client.RestTemplate;

import com.example.bankapplication.constants.AppConstants;
import com.example.bankapplication.dto.AddAccountDTO;
import com.example.bankapplication.dto.AddaccountResponseDTO;
import com.example.bankapplication.dto.FavAccountDTO;
import com.example.bankapplication.dto.IBANOwnerResponse;
import com.example.bankapplication.dto.IBANResponse;
import com.example.bankapplication.dto.LoginDTO;
import com.example.bankapplication.dto.ResponseDTO;
import com.example.bankapplication.entity.Account;
import com.example.bankapplication.entity.Customer;
import com.example.bankapplication.exception.AccountNotFoundException;
import com.example.bankapplication.exception.CustomerNotFoundException;
import com.example.bankapplication.exception.FavAccountExistsException;
import com.example.bankapplication.exception.FavAccountLimitReachedException;
import com.example.bankapplication.exception.InvalidCustomerException;
import com.example.bankapplication.exception.RestTemplateException;
import com.example.bankapplication.repository.AccountRepository;
import com.example.bankapplication.repository.CustomerRepository;

@Service
public class CustomerAccountServiceImpl implements CustomerAccountService {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(CustomerAccountServiceImpl.class);

	@Autowired
	CustomerRepository customerRepository;

	@Autowired
	AccountRepository accountRepository;

	@Override
	public List<FavAccountDTO> fetchCustomerFavAccounts(long customerId)
			throws AccountNotFoundException, CustomerNotFoundException {

		logger.info("CustomerAccountServiceImpl: fetchCustomerFavAccounts ");

		Optional<Customer> cust = customerRepository.findById(customerId);

		if (!cust.isPresent()) {
			logger.info("CustomerAccountServiceImpl: customer id not found ");
			throw new CustomerNotFoundException(AppConstants.CUSTOMER_ID_NOT_FOUND);
		}

		List<FavAccountDTO> falist = new ArrayList<>();

		List<Account> favaclist = cust.get().getAccounts();

		favaclist.forEach(a -> {
			FavAccountDTO fa = new FavAccountDTO();
			BeanUtils.copyProperties(a, fa);
			falist.add(fa);
		});

		return falist;
	}

	@Transactional
	@Override
	public AddaccountResponseDTO addAccount(long customerId, @Valid AddAccountDTO addAccountDto)
			throws FavAccountLimitReachedException, CustomerNotFoundException, FavAccountExistsException,
			RestTemplateException {

		logger.info("CustomerAccountServiceImpl: addAccount ");

		Optional<Customer> cust = customerRepository.findById(customerId);
		if (!cust.isPresent()) {

			logger.info("CustomerAccountServiceImpl: addAccount: customer id not found ");
			throw new CustomerNotFoundException(AppConstants.CUSTOMER_ID_NOT_FOUND);
		}

		List<Account> aclist = cust.get().getAccounts();

		Optional<Account> acc = accountRepository.checkIbannumber(addAccountDto.getIBAN());

		if (acc.isPresent()) {

			if (aclist.contains(acc.get())) {

				logger.info("CustomerAccountServiceImpl: addAccount: iban already added by this user");
				throw new FavAccountExistsException(AppConstants.IBAN_NUMBER_ALREADY_EXISTS);
			}
			if (aclist.size() >= 9) {

				logger.info(
						"CustomerAccountServiceImpl: addAccount: max number of favorite accounts already added by this customer");
				throw new FavAccountLimitReachedException(AppConstants.FAV_ACCOUNT_LIMIT_REACHED);
			}

			Account ac = new Account();
			BeanUtils.copyProperties(acc.get(), ac);
			aclist.add(ac);
			cust.get().setAccounts(aclist);
			customerRepository.save(cust.get());
			AddaccountResponseDTO fad = new AddaccountResponseDTO();
			BeanUtils.copyProperties(ac, fad);
			fad.setMessage(AppConstants.FAV_ACCOUNT_ADDED);
			fad.setStatusCode(201);

			logger.info("CustomerAccountServiceImpl: addAccount: new favorite account added ");
			return fad;

		}

		logger.info(
				"CustomerAccountServiceImpl: addAccount: calling external services since iban entry not found in our database ");

		try {
			RestTemplate restTemplate = new RestTemplate();

			IBANResponse response1 = restTemplate.getForObject("http://localhost:9080/api/bankinfo/{IBAN}",
					IBANResponse.class, addAccountDto.getIBAN());

			IBANOwnerResponse response2 = restTemplate.getForObject(response1.getBankAccountServiceURL() + "/{IBAN}",
					IBANOwnerResponse.class, addAccountDto.getIBAN());

			if (aclist.size() >= 9) {

				logger.info(
						"CustomerAccountServiceImpl: addAccount: max number of favorite accounts already added by this customer");
				throw new FavAccountLimitReachedException(AppConstants.FAV_ACCOUNT_LIMIT_REACHED);
			}
			
			Account ac = new Account();
			ac.setAccountOwnerName(response2.getOwnerName());
			ac.setBankName(response1.getBankName());
			ac.setIBAN(addAccountDto.getIBAN());
			aclist.add(ac);
			cust.get().setAccounts(aclist);
			customerRepository.save(cust.get());

			AddaccountResponseDTO fad = new AddaccountResponseDTO();
			BeanUtils.copyProperties(ac, fad);
			fad.setMessage(AppConstants.FAV_ACCOUNT_ADDED);
			fad.setStatusCode(201);

			logger.info(
					"CustomerAccountServiceImpl: addAccount: new favorite account added after calling external api's ");
			return fad;

		}

		catch (HttpClientErrorException | HttpServerErrorException httpClientOrServerExc) {

			logger.info(
					"CustomerAccountServiceImpl: addAccount: Handling exception when invalid iba is passed by customer");

			if (HttpStatus.NOT_FOUND.equals(httpClientOrServerExc.getStatusCode())) {
				throw new RestTemplateException(AppConstants.REST_SERVICE_CALL_NOT_FOUND);
			}
			if (HttpStatus.BAD_REQUEST.equals(httpClientOrServerExc.getStatusCode())) {
				throw new RestTemplateException(AppConstants.REST_SERVICE_CALL_BAD_REQUEST);
			}
			throw new RestTemplateException(AppConstants.REST_SERVICE_CALL_ERROR);
		}

	}

	@Override
	public List<FavAccountDTO> fetchCustomerFavAccountsPaginated(long customerId, int page)
			throws AccountNotFoundException, CustomerNotFoundException {

		logger.info("CustomerAccountServiceImpl: fetchCustomerFavAccountsPaginated:");
		long size = 3;

		List<FavAccountDTO> flist = fetchCustomerFavAccounts(customerId);

		Comparator<FavAccountDTO> compareByOwnerName = (FavAccountDTO f1, FavAccountDTO f2) -> f1.getAccountOwnerName()
				.compareTo(f2.getAccountOwnerName());
		Collections.sort(flist, compareByOwnerName);

		return flist.stream().skip(size * page).limit(size).collect(Collectors.toList());

	}

	@Transactional
	@Override
	public ResponseDTO deleteFavAccount(long customerId, long accountId)
			throws CustomerNotFoundException, AccountNotFoundException, InvalidCustomerException {

		logger.info("CustomerAccountServiceImpl: deleteFavAccount:");

		Optional<Account> acc = accountRepository.findById(accountId);
		Optional<Customer> cust = customerRepository.findById(customerId);
		if (!cust.isPresent() || !acc.isPresent()) {

			logger.info("CustomerAccountServiceImpl: deleteFavAccount: account not found in our database");
			throw new CustomerNotFoundException(AppConstants.ACCOUNT_NOT_FOUND);
		}

		List<Account> aclist = cust.get().getAccounts();

		if (!aclist.contains(acc.get())) {

			logger.info("CustomerAccountServiceImpl: deleteFavAccount: account not found in customer favlist");
			throw new CustomerNotFoundException(AppConstants.ACCOUNT_NOT_FOUND);

		}

		aclist.remove(aclist.indexOf(acc.get()));
		customerRepository.save(cust.get());

		ResponseDTO rs = new ResponseDTO();
		rs.setMessage(AppConstants.FAV_ACC_DELETED);
		rs.setStatusCode(201);

		logger.info(
				"CustomerAccountServiceImpl: deleteFavAccount: account deleted from customer favorite account list");

		return rs;
	}

	@Override
	public ResponseDTO customerLogin(@Valid LoginDTO loginDto, HttpServletRequest request)
			throws InvalidCustomerException {

		logger.info("CustomerAccountServiceImpl: customerLogin ");

		Optional<Customer> custLoginid = customerRepository.validateCustomer(loginDto.getCustomerLoginId());
		if (!custLoginid.isPresent()) {
			throw new InvalidCustomerException(AppConstants.INVALID_CUSTOMER);
		}

		ResponseDTO result = new ResponseDTO();
		result.setMessage(AppConstants.LOGIN_SUCCESS);
		result.setStatusCode(200);

		return result;
	}

}
