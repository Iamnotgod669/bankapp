package com.example.bankapplication.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.stereotype.Repository;

import com.example.bankapplication.entity.Customer;

@Repository
public interface CustomerRepository extends JpaRepository<Customer,Long> {
	
	@Query(value = "select * from customer where customer_login_id = ?1", nativeQuery = true)
	Optional<Customer> validateCustomer(String custLoginId);
	
	
	@Query(value = "select * from customer_account where customer_id = ?1", nativeQuery = true)
	Optional<List<Customer>> fetchCustAccounts(long custId);
	
	
	
	

}

