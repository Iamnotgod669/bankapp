package com.example.bankapplication.repository;

import java.util.List;
import java.util.Optional;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;

import com.example.bankapplication.entity.Account;

public interface AccountRepository extends JpaRepository<Account,Long> {
	
	@Query(value = "select * from account where customer_id = ?1", nativeQuery = true)
	Optional<List<Account>> fetchFavAccounts(long custId);
	
	@Query(value = "select * from account where iban = ?1", nativeQuery = true)
	Optional<Account> checkIbannumber(String iban);
	
	

}
