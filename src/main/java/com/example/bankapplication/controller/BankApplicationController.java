package com.example.bankapplication.controller;

import java.util.List;

import javax.servlet.http.HttpServletRequest;
import javax.validation.Valid;

import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.example.bankapplication.dto.AddAccountDTO;
import com.example.bankapplication.dto.AddaccountResponseDTO;
import com.example.bankapplication.dto.FavAccountDTO;
import com.example.bankapplication.dto.LoginDTO;
import com.example.bankapplication.dto.ResponseDTO;
import com.example.bankapplication.exception.AccountNotFoundException;
import com.example.bankapplication.exception.CustomerNotFoundException;
import com.example.bankapplication.exception.FavAccountExistsException;
import com.example.bankapplication.exception.FavAccountLimitReachedException;
import com.example.bankapplication.exception.InvalidCustomerException;
import com.example.bankapplication.exception.RestTemplateException;
import com.example.bankapplication.service.CustomerAccountService;

import io.swagger.annotations.Api;

@RestController
@RequestMapping("/")
@Api(value = "BankApplicationController")
public class BankApplicationController {

	private static final org.slf4j.Logger logger = LoggerFactory.getLogger(BankApplicationController.class);

	@Autowired
	CustomerAccountService customerAccountService;

	/**
	 * customerLogin
	 * 
	 * @param loginDto
	 * @author Vibhuti
	 * @param request
	 * @return
	 * @throws InvalidCustomerException
	 */
	@PostMapping("/login")
	public ResponseEntity<ResponseDTO> customerLogin(@Valid @RequestBody LoginDTO loginDto, HttpServletRequest request)
			throws InvalidCustomerException {
		logger.info("BankApplicationController: customerLogin ");

		return new ResponseEntity<>(customerAccountService.customerLogin(loginDto, request), HttpStatus.OK);
	}

	/**
	 * getCustomerFavAccounts
	 * 
	 * @param customerId
	 * 
	 * @author SriKrishna
	 * 
	 * @return
	 * @throws AccountNotFoundException
	 * @throws CustomerNotFoundException
	 */
	@GetMapping("/customers/{customerId}/accounts")
	public ResponseEntity<List<FavAccountDTO>> getCustomerFavAccounts(@PathVariable("customerId") long customerId,
			@RequestParam int page) throws AccountNotFoundException, CustomerNotFoundException {
		logger.info("BankApplicationController: getCustomerFavAccounts ");
		if (page > 2) {
			return new ResponseEntity<>(customerAccountService.fetchCustomerFavAccounts(customerId), HttpStatus.OK);
		}
		return new ResponseEntity<>(customerAccountService.fetchCustomerFavAccountsPaginated(customerId, page),
				HttpStatus.OK);

	}

	/**
	 * addFavoriteAccount
	 * 
	 * @param customerId
	 * 
	 * @author SriKrishna
	 * 
	 * @param AddAccountDTO
	 * @return
	 * @throws FavAccountExistsException
	 * @throws CustomerNotFoundException
	 * @throws RestTemplateException
	 * @throws FavAccountLimitReachedException
	 */
	@PostMapping("/customer/{customerId}/accounts")
	public ResponseEntity<AddaccountResponseDTO> addFavoriteAccount(@PathVariable("customerId") long customerId,
			@Valid @RequestBody AddAccountDTO addAccountDto) throws RestTemplateException,
			FavAccountLimitReachedException, CustomerNotFoundException, FavAccountExistsException {
		logger.info("BankApplicationController: addFavoriteAccount ");
		return new ResponseEntity<>(customerAccountService.addAccount(customerId, addAccountDto), HttpStatus.OK);
	}

	/**
	 * deleteFavoriteAccount
	 * 
	 * @param customerId
	 * 
	 * @param accountId
	 * @author Vibhuti
	 * @return
	 * @throws AccountNotFoundException
	 * @throws CustomerNotFoundException
	 * 
	 */
	@DeleteMapping("/customer/{customerId}/accounts/{accountId}")
	public ResponseEntity<ResponseDTO> deleteFavoriteAccount(@PathVariable("customerId") long customerId,
			@PathVariable("accountId") long accountId)
			throws CustomerNotFoundException, AccountNotFoundException, InvalidCustomerException {
		logger.info("BankApplicationController: deleteFavoriteAccount ");
		return new ResponseEntity<>(customerAccountService.deleteFavAccount(customerId, accountId), HttpStatus.OK);
	}

}
