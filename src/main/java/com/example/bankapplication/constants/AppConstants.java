package com.example.bankapplication.constants;

public final class AppConstants {
	
	
	
	private AppConstants() {
		
		
	}
	public static final String FAV_ACCOUNT_LIMIT_REACHED = "you cannot add more than 9 favorite accounts";
	public static final String CUSTOMER_NOT_FOUND = "Please enter proper login id";
	public static final String IBAN_NUMBER_ALREADY_EXISTS = "You have already added this account, please enter different IBAN account number";
	public static final String FAV_ACCOUNT_ADDED = "favorite account added successfully";
	public static final String CUSTOMER_ID_NOT_FOUND = "Invalid Customer ID";
	public static final String REST_SERVICE_CALL_ERROR = "Service call returned a error";
	public static final String INVALID_CUSTOMER = "Invalid Customer ID";
    public static final String INVALID_ACCOUNTNUMBER = "Invalid AccountNumber";
    public static final String INVALID_DETAILS ="you are not authorized to delete, please login with customer ID";
	public static final String REST_SERVICE_CALL_BAD_REQUEST = "This Iban in Invalid. Please enter a valid Iban";
	public static final String REST_SERVICE_CALL_NOT_FOUND = "This Iban does not exist. Please enter valid Iban";
	public static final String LOGIN_SUCCESS = "Login Successful";
	public static final String ACCOUNT_NOT_FOUND = "Account not found";
	public static final String FAV_ACC_DELETED = "Successfully deleted favorite account";
	public static final String FAV_ACCOUNT_NOT_FOUND = "No favorite accounts added click add favorite option to add ne wfavorite account";
}
