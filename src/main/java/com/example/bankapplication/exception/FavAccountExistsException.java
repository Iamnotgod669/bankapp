package com.example.bankapplication.exception;

public class FavAccountExistsException extends Exception {


	
	private static final long serialVersionUID = 1L;


	public FavAccountExistsException(String message) {
		super(message);
	}


}
