package com.example.bankapplication.exception;

import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.ControllerAdvice;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.servlet.mvc.method.annotation.ResponseEntityExceptionHandler;



@ControllerAdvice
public class GlobalExceptionHandler extends ResponseEntityExceptionHandler {
	
	
	@ExceptionHandler(CustomerNotFoundException.class)
	public ResponseEntity<ResponseStatus> customException(CustomerNotFoundException ex) {

		ResponseStatus errorResponse = new ResponseStatus();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatuscode(404);

		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}
	
	@ExceptionHandler(AccountNotFoundException.class)
	public ResponseEntity<ResponseStatus> customException(AccountNotFoundException ex) {

		ResponseStatus errorResponse = new ResponseStatus();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatuscode(404);

		return new ResponseEntity<>(errorResponse, HttpStatus.NOT_FOUND);

	}
	
	@ExceptionHandler(FavAccountExistsException.class)
	public ResponseEntity<ResponseStatus> customException(FavAccountExistsException ex) {

		ResponseStatus errorResponse = new ResponseStatus();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatuscode(400);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}
	
	
	@ExceptionHandler(FavAccountLimitReachedException.class)
	public ResponseEntity<ResponseStatus> customException(FavAccountLimitReachedException ex) {

		ResponseStatus errorResponse = new ResponseStatus();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatuscode(400);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}
	
	
	@ExceptionHandler(RestTemplateException.class)
	public ResponseEntity<ResponseStatus> customException(RestTemplateException ex) {

		ResponseStatus errorResponse = new ResponseStatus();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatuscode(400);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}
	
	
	@ExceptionHandler(InvalidCustomerException.class)
	public ResponseEntity<ResponseStatus> customException(InvalidCustomerException ex) {

		ResponseStatus errorResponse = new ResponseStatus();

		errorResponse.setMessage(ex.getMessage());
		errorResponse.setStatuscode(400);

		return new ResponseEntity<>(errorResponse, HttpStatus.BAD_REQUEST);

	}
	
	
	
	
	
	
}
