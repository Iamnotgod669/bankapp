package com.example.bankapplication.exception;

public class RestTemplateException extends Exception {


	
	private static final long serialVersionUID = 1L;


	public RestTemplateException(String message) {
		super(message);
	}


}
