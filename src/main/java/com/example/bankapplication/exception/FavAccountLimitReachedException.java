package com.example.bankapplication.exception;

public class FavAccountLimitReachedException extends Exception {


	
	private static final long serialVersionUID = 1L;


	public FavAccountLimitReachedException(String message) {
		super(message);
	}


}