package com.example.bankapplication.entity;

import java.util.List;

import javax.persistence.CascadeType;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
@Entity
@Table(name="customer")
public class Customer {
	
	
	        @Id
			@GeneratedValue(strategy= GenerationType.AUTO)
			private long customerId;
			
			private String customerName;
			
			private String customerLoginId;
			
			
			@ManyToMany(fetch = FetchType.EAGER,cascade = { CascadeType.ALL, CascadeType.REFRESH })
			@JoinTable(name = "customer_account", joinColumns = { @JoinColumn(name = "customer_id") }, inverseJoinColumns = {
					@JoinColumn(name = "account_id") })
			private List<Account> accounts;
			
}
