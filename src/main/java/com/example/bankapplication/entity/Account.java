package com.example.bankapplication.entity;

import javax.persistence.Entity;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.Table;

import lombok.Data;
import lombok.Getter;
import lombok.Setter;

@Data
@Getter
@Setter
@Entity
@Table(name="account")
public class Account {
	
	    @Id
		@GeneratedValue(strategy= GenerationType.AUTO)
		private long accountId;
		
		private String IBAN;
		
		private String accountOwnerName;
		
		private String bankName;
		
		
		 
		 
		 
		
		
		

	

}
