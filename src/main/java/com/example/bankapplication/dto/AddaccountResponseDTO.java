package com.example.bankapplication.dto;


import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class AddaccountResponseDTO extends ResponseDTO{
	
private String IBAN;
	
	private String accountOwnerName;
	
	private String bankName;

}
