package com.example.bankapplication.dto;

import lombok.AllArgsConstructor;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.Setter;

@AllArgsConstructor
@NoArgsConstructor
@Getter
@Setter
public class FavAccountDTO  {
	
	
	private String IBAN;
	
	private String accountOwnerName;
	
	private String bankName;

}
