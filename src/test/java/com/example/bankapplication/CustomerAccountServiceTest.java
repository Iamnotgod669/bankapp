package com.example.bankapplication;

import static org.junit.Assert.assertThrows;
import static org.junit.jupiter.api.Assertions.assertEquals;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

import javax.servlet.http.HttpServletRequest;

import org.junit.Assert;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.Mockito;
import org.springframework.beans.BeanUtils;
import org.springframework.boot.test.context.SpringBootTest;

import com.example.bankapplication.constants.AppConstants;
import com.example.bankapplication.dto.AddAccountDTO;
import com.example.bankapplication.dto.AddaccountResponseDTO;
import com.example.bankapplication.dto.FavAccountDTO;
import com.example.bankapplication.dto.LoginDTO;
import com.example.bankapplication.dto.ResponseDTO;
import com.example.bankapplication.entity.Account;
import com.example.bankapplication.entity.Customer;
import com.example.bankapplication.exception.AccountNotFoundException;
import com.example.bankapplication.exception.CustomerNotFoundException;
import com.example.bankapplication.exception.FavAccountExistsException;
import com.example.bankapplication.exception.FavAccountLimitReachedException;
import com.example.bankapplication.exception.InvalidCustomerException;
import com.example.bankapplication.exception.RestTemplateException;
import com.example.bankapplication.repository.AccountRepository;
import com.example.bankapplication.repository.CustomerRepository;
import com.example.bankapplication.service.CustomerAccountServiceImpl;

@SpringBootTest
public class CustomerAccountServiceTest {

	@InjectMocks
	CustomerAccountServiceImpl customerAccountServiceImpl;

	@Mock
	CustomerRepository customerRepository;

	@Mock
	AccountRepository accountRepository;

	List<Account> aclist = new ArrayList<>();
	List<Account> aclist1 = new ArrayList<>();
	List<Account> aclist2 = new ArrayList<>();
	List<Account> aclist3 = new ArrayList<>();
	AddAccountDTO aad = new AddAccountDTO();
	AddAccountDTO aad1 = new AddAccountDTO();
	Customer c = new Customer();
	Customer c1 = new Customer();
	Customer c2 = new Customer();
	Customer c3 = new Customer();
	Account a = new Account();
	Account a1 = new Account();
	long on = 1;

	@BeforeEach
	public void init() {

		aad1.setIBAN("FR33123600033300002");
		aad.setIBAN("ABCD123456789");
		a1.setAccountOwnerName("zzz");
		a.setAccountId(1);
		a.setAccountOwnerName("owner");
		a.setBankName("dfg");
		a.setIBAN("ABCD123456789");
		aclist.add(a);
		aclist1.add(a1);
		aclist1.add(a);
		aclist1.add(a);
		aclist1.add(a);
		aclist2.add(a1);
		
        while(aclist3.size() <= 9) {
        	aclist3.add(a);
        }

		c1.setAccounts(aclist1);
		c.setCustomerId(1);
		c.setCustomerLoginId("abc");
		c.setCustomerName("sri");
		c.setAccounts(aclist);
		c2.setCustomerId(2);
		c2.setCustomerLoginId("loginid");
		c2.setCustomerName("krishna");
		c2.setAccounts(aclist2);
		BeanUtils.copyProperties(c2, c3);
		c3.setAccounts(aclist3);

	}

	@Test
	public void TestfetchCustomerFavAccounts() throws AccountNotFoundException, CustomerNotFoundException {

		Mockito.when(customerRepository.findById(on)).thenReturn(Optional.of(c));

		List<FavAccountDTO> result = customerAccountServiceImpl.fetchCustomerFavAccounts(on);

		Assert.assertEquals("ABCD123456789", result.get(0).getIBAN());

	}

	@Test
	public void TestfetchCustomerFavAccountsPaginated() throws AccountNotFoundException, CustomerNotFoundException {

		Mockito.when(customerRepository.findById(on)).thenReturn(Optional.of(c1));

		List<FavAccountDTO> result = customerAccountServiceImpl.fetchCustomerFavAccountsPaginated(on, 0);

		Assert.assertEquals(3, result.size());

		List<FavAccountDTO> result1 = customerAccountServiceImpl.fetchCustomerFavAccountsPaginated(on, 1);

		Assert.assertEquals(1, result1.size());

		Assert.assertEquals("zzz", result1.get(0).getAccountOwnerName());

	}

	@Test
	public void TestdeleteFavAccount()
			throws CustomerNotFoundException, AccountNotFoundException, InvalidCustomerException {

		Mockito.when(customerRepository.findById(on)).thenReturn(Optional.of(c));

		Mockito.when(accountRepository.findById(on)).thenReturn(Optional.of(a));

		ResponseDTO rd = customerAccountServiceImpl.deleteFavAccount(on, on);

		Assert.assertEquals(AppConstants.FAV_ACC_DELETED, rd.getMessage());

	}

	@Test
	public void TestaddAccount() throws FavAccountLimitReachedException, CustomerNotFoundException,
			FavAccountExistsException, RestTemplateException {

		Mockito.when(customerRepository.findById(on)).thenReturn(Optional.of(c2));

		Mockito.when(accountRepository.checkIbannumber("ABCD123456789")).thenReturn(Optional.of(a));

		AddaccountResponseDTO result = customerAccountServiceImpl.addAccount(on, aad);

		Assert.assertEquals("owner", result.getAccountOwnerName());

	}

	@Test
	public void TestaddAccount1() throws FavAccountLimitReachedException, CustomerNotFoundException,
			FavAccountExistsException, RestTemplateException {

		Mockito.when(customerRepository.findById(on)).thenReturn(Optional.of(c2));

		Mockito.when(accountRepository.checkIbannumber("ABCD123456789")).thenReturn(Optional.of(a1));

		AddaccountResponseDTO result = customerAccountServiceImpl.addAccount(on, aad1);

		Assert.assertEquals("Martin Berrote", result.getAccountOwnerName());

	}

	@Test
	public void TestaddAccountNegativeResttemplateError() throws FavAccountLimitReachedException,
			CustomerNotFoundException, FavAccountExistsException, RestTemplateException {

		Mockito.when(customerRepository.findById(on)).thenReturn(Optional.of(c2));

		Optional<Account> ao = Optional.empty();
		Mockito.when(accountRepository.checkIbannumber("ABCD123456789")).thenReturn(ao);

		assertThrows(RestTemplateException.class, () -> customerAccountServiceImpl.addAccount(on, aad));

	}

	@Test
	public void testLogin() throws InvalidCustomerException {
		LoginDTO loginDto = new LoginDTO();

		loginDto.setCustomerLoginId("Asd123P");

		Customer customer = new Customer();
		customer.setCustomerLoginId("Asd123P");
		HttpServletRequest request = null;
		Mockito.when(customerRepository.validateCustomer("Asd123P")).thenReturn(Optional.of(customer));
		assertEquals(200, customerAccountServiceImpl.customerLogin(loginDto, request).getStatusCode());

	}

	@Test
	public void testLoginCustomerNotFound() {
		LoginDTO loginDto = new LoginDTO();

		loginDto.setCustomerLoginId("Asd123P");

		Customer customer = new Customer();
		customer.setCustomerLoginId("Asd123P");
		HttpServletRequest request = null;

		Mockito.when(customerRepository.validateCustomer("Asd123")).thenReturn(Optional.of(customer));
		assertThrows(InvalidCustomerException.class, () -> customerAccountServiceImpl.customerLogin(loginDto, request));

	}

	@Test
	public void TestaddAccountNegativeMaxnumberofFavAccounts() throws FavAccountLimitReachedException,
			CustomerNotFoundException, FavAccountExistsException, RestTemplateException {

		Mockito.when(customerRepository.findById(on)).thenReturn(Optional.of(c3));

		Optional<Account> ao = Optional.empty();
		Mockito.when(accountRepository.checkIbannumber("FR33123600033300002")).thenReturn(ao);

		assertThrows(FavAccountLimitReachedException.class, () -> customerAccountServiceImpl.addAccount(on, aad1));

	}

	@Test
	public void TestdeleteFavAccountnNegative()
			throws CustomerNotFoundException, AccountNotFoundException, InvalidCustomerException {

		Mockito.when(customerRepository.findById(on)).thenReturn(Optional.of(c));

		Optional<Account> ao = Optional.empty();
		Mockito.when(accountRepository.findById(on)).thenReturn(ao);

		assertThrows(CustomerNotFoundException.class, () -> customerAccountServiceImpl.deleteFavAccount(on, on));

	}

}
